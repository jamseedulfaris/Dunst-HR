// main js
$(document).ready(function(){
new WOW().init();
});
//menu
$("#toggle").click(function() {
    $(this).toggleClass("open");
    $("#menu").toggleClass("opened");
});


// scroll top

$(window).scroll(function() {
  if ($(this).scrollTop() > 80) {      $('#wrap').slideDown('slow').css('display','block');    }    else {      $('#wrap').slideUp('slow');  }
});

//On click arrow scroll down requird script
window.smoothScroll = function(target) {
    var scrollContainer = target;
    do { //find scroll container
        scrollContainer = scrollContainer.parentNode;
        if (!scrollContainer) return;
        scrollContainer.scrollTop += 1;
    } while (scrollContainer.scrollTop == 0);

    var targetY = 0;
    do { //find the top of target relatively to the container
        if (target == scrollContainer) break;
        targetY += target.offsetTop;
    } while (target = target.offsetParent);

    scroll = function(c, a, b, i) {
        i++; if (i > 30) return;
        c.scrollTop = a + (b - a) / 30 * i;
        setTimeout(function(){ scroll(c, a, b, i); }, 20);
    }
    // start scrolling
    scroll(scrollContainer, scrollContainer.scrollTop, targetY, 0);
}


//scroll top
    $(document).ready(function(){
        //Check to see if the window is top if not then display button
        $(window).scroll(function(){
            if ($(this).scrollTop() > 600) {
                $('.scrollToTop').fadeIn();
            } else {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function(){
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });

    });

// testimonial scrip
$(function () {
$('#quote-carousel').carousel({
    interval: false
});
$('#quote-carousel').on('slid.bs.carousel', function (e) {
    if ($('.carousel-inner .item:last').hasClass('active')) {
        $('#quote-carousel').carousel('pause');
    }
    if ($('.carousel-inner .item:first').hasClass('active')) {
        $('#quote-carousel').carousel('cycle');
    }
});
});



// budget form captch
$("form#budget_form").submit(function(event) {

   var recaptcha = $("#g-recaptcha-response").val();

   if (recaptcha === "") {

      event.preventDefault();

      alert("Please check the recaptcha!");

   } else{

    setInterval(mytimer,10000000000000000000000000000000000000000000000000000000000000);

    function mytimer()
    {
    $('#message').css('display','block');
    }
   }
});
